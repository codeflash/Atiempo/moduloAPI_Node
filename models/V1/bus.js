var mongoose = require('mongoose')
   ,Schema = mongoose.Schema
   ,ObjectId = Schema.ObjectId
   ,Mixed = Schema.Mixed;

var bus = new Schema({
    id: ObjectId,
    name: {type: String},
    description: {type: String}
});

module.exports = mongoose.model('bus', bus);