
var json_response = function(){
    var content, errors = [];
    var meta = {};

    return {
        data_response: function(data){
            content =  data
        },
        meta_response: function() {
            return{
                statusCode: function (code) {
                    meta = {
                        statusCode: code
                    }
                }
            }
        },
        errors_response: function(){
            return errors;

        },
        json_parse: function () {
            return{
                meta: meta,
                data: content,
                errors: errors
            }
        }
    }
};

module.exports ={
    json_response: json_response()
};

