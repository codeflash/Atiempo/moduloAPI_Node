var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
var geolib = require('geolib'),
    _ = require("underscore");
var Route_schema = new Schema({
    id: ObjectId,
    description: { type: String },
    loc: {
        type: { type: String, default: "MultiPoint" },
        coordinates: { type: Array }
    }
});

Route_schema.index({ 'loc': '2dsphere' });

var Route = mongoose.model('route', Route_schema, 'routes');


function forRoutes(mayor, menor) {
    var routesMatch = [];
    for(var x = 0 ; x < mayor.length; x++){
        for(var y = 0 ; y < menor.length; y++){
            if(mayor[x]._id.toString() === menor[y]._id.toString()){
                routesMatch.push(mayor[x]);
            }
        }
    }
    return routesMatch;
}

function getDistanceStartAndEnd(routesMatch , confStart, confEnd) {
    var routes = []
    for(var i = 0; i < routesMatch.length; i++){
        var coordinates = routesMatch[i].loc.coordinates;
        var _distancesStart = [];
        var _distancesEnd = [];
        for(var y = 0; y < coordinates.length; y++) {
            _distancesStart.push(geolib.getDistance(
                {latitude: confStart.coordinate[0], longitude: confStart.coordinate[1]},
                {latitude: coordinates[y][0], longitude: coordinates[y][1]}
            ));
            _distancesEnd.push(geolib.getDistance(
                {latitude: confEnd.coordinate[0], longitude: confEnd.coordinate[1]},
                {latitude: coordinates[y][0], longitude: coordinates[y][1]}
            ));
        }
        routes.push({
            distancesStart: _distancesStart,
            distancesEnd: _distancesEnd
        })
    }
    return routes;
}

function getNearPoint(distances){
    var menor = distances[0];
    var index;
    for(var i = 0 ; i < distances.length; i++){
        if(distances[i] < menor){
            menor = distances[i];
            index = i;
        }
    }

    return {
        distance: menor,
        index: index
    }
}

module.exports = {
    findByCoordinate : function (conf, callback) {
        Route.find({
            loc: {
                $nearSphere: {
                    $geometry: {
                        type: "Point",
                        coordinates: conf.coordinate
                    },
                    $maxDistance: conf.distance
                }
            }
        }, function(error , routes){
            callback(error, routes);

        });
    },

    getMatchRoutes: function(startRoutes, endRoutes , confStart , confEnd){

        if(startRoutes.length === 0 || endRoutes.length === 0){
            return [];
        }


        var routesMatch = forRoutes(endRoutes, startRoutes);

        var routes = getDistanceStartAndEnd(routesMatch, confStart, confEnd);
        var finalRoutes = [];
        for(var i = 0; i < routes.length; i++){
            var distancesEnd = routes[i].distancesEnd;
            var distancesStart = routes[i].distancesStart;
            var pointEnd = getNearPoint(distancesEnd);
            var pointStart = getNearPoint(distancesStart);

            if(pointStart.index < pointEnd.index){
                var route = {
                    id : routesMatch[i]._id,
                    description: routesMatch[i].description,
                    coordinates: routesMatch[i].loc.coordinates.slice(pointStart.index, pointEnd.index + 1)
                };
                finalRoutes.push(route);
            }
        }

        return finalRoutes;

    }
};

