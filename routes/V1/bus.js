var bus = require('../../controllers/v1/bus');
var express    = require('express');
var router = express.Router();


router.get('/buses/:id', bus.findById);

router.put('/buses/:id', bus.update);

router.delete('/buses/:id', bus.delete);

router.get('/buses', bus.findAll);

router.post('/buses', bus.add);

module.exports = router;
