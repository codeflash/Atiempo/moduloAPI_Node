var trip = require('../../controllers/v1/trip');
var express = require('express');
var router = express.Router();

router.get('/trips', trip.search);

module.exports = router;