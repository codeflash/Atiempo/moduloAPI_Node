var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'jade');

var port = process.env.PORT || 3000;

var router = express.Router();

var bus = require('./routes/v1/bus');

var route = require('./routes/v1/route');

var search = require('./routes/v1/trip');

router.get('/', function(req, res) {
    res.render('routes');
});

router.get('/docs', function(req, res) {
    res.render('docs');
});

app.use('/', router);

// Routes API v1
app.use('/api/v1', bus);
//app.use('/api/v1', route);
app.use('/api/v1', search);

app.use(function(req, res) {
    res.status(404).send({ url: req.originalUrl + ' not found' })
});

mongoose.connect('mongodb://localhost/atiempo', function(err, res) {
    if (err) {
        console.log('ERROR: connecting to Database. ' + err);
    }
    app.listen(port, function() {
        console.log("Node server running on http://localhost:" + port);
    });
});