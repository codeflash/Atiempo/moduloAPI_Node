var Bus = require('../../models/v1/bus');

module.exports = {
    findById: function(req, res , next){
        Bus.findById(req.params.id, function(err, bus){
            res.status(200).send(bus);
        });
    },
    findAll: function(req, res , next){
        Bus.find(function(err, buses){
            res.status(200).jsonp(buses);
        });
    },
    add: function(req, res, next){
        var bus = new Bus({
            name: req.body.name,
            description: req.body.description
        })
        bus.save(function(err,bus){
            res.status(200).jsonp(bus);
        })
    },
    update: function(req, res, next){
        Bus.findById(req.params.id, function(err, bus){
            bus.name = req.body.name;
            bus.description = req.body.description;
            bus.save(function(err,bus){
                res.status(200).jsonp(bus);
            })
        });
    },
    delete: function(req, res, next){
        Bus.findById(req.params.id, function(err, bus){
            bus.name = req.body.name;
            bus.description = req.body.description;
            bus.remove(function(err){
                res.status(200).jsonp();
            });
        });
    },
}
