var Route = require('../../models/V1/route');
var json_response = require('../../models/V1/ApiResponse');





module.exports = {
    search: function(req, res) {
        var response = json_response.json_response;
        if
        (
            req.query.start === undefined ||
            req.query.end === undefined
        )
        {
            response.meta_response().statusCode(400);
            response.errors_response().push("Debe ingresar el campo start o end");
            res.jsonp(response.json_parse());
            return
        }
        var start = req.query.start.split(',') || "0,0".split(','),
            end = req.query.end.split(',') || "0,0".split(','),
            distance = req.query.distance || 500,
            transbord = req.query.transbord || 1;

        var confStart = {
            coordinate: [parseFloat(start[0]) , parseFloat(start[1])],
            distance: distance
        };
        var confEnd = {
            coordinate: [parseFloat(end[0]) , parseFloat(end[1])],
            distance: distance
        };
        Route.findByCoordinate(confStart, function(error, routesStart){
            if(error){
                return console.log(error);
            }
            Route.findByCoordinate(confEnd, function (error, routesEnd) {
                if(error){
                    return console.log(error);
                }
                var finalRoutes = Route.getMatchRoutes(routesStart, routesEnd, confStart , confEnd);

                response.meta_response().statusCode(200);
                response.data_response(finalRoutes);
                res.jsonp(response.json_parse());
            });

        });
    }
};